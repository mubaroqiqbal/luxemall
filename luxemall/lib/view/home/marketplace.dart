import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:luxemall/util/constants_var.dart';
import 'package:luxemall/view/Home/components/body.dart';
import 'package:luxemall/view/home/bloc_builder/categories_bloc_builder.dart';
import 'package:luxemall/view/home/bloc_builder/products_bloc_builder.dart';

class Marketplace extends StatefulWidget {
  @override
  _Marketplace createState() => _Marketplace();
}

class _Marketplace extends State<Marketplace>{
  final StreamController<bool> streamController = StreamController<bool>.broadcast();

  @override
  void initState() {
    super.initState();

    ProdutsBlocBuilder.productsCubit.getProducts(false);
    CategoriesBlocBuilder.categoriesCubit.getCategories();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryGold,
        elevation: 0,
        titleSpacing: 0,
        title: Padding(
          padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
          child: GestureDetector(
            onTap: (){
              ProdutsBlocBuilder.productsCubit.finish();
              CategoriesBlocBuilder.categoriesCubit.finish();

              ProdutsBlocBuilder.productsCubit.getProducts(false);
              CategoriesBlocBuilder.categoriesCubit.getCategories();
            },
            child: Text(
              "LUXEMALL",
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: SvgPicture.asset(
              "assets/icons/cart.svg",
              // By default our  icon color is white
              color: Colors.white,
            ),
            onPressed: () {},
          ),
          SizedBox(width: defaultPadding / 2)
        ],
      ),
      backgroundColor: Colors.white,
      body: Body(),
    );
  }

}
