import 'package:flutter/material.dart';
import 'package:luxemall/cubit/categories_cubit.dart';
import 'package:luxemall/data/data_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategoriesBlocBuilder{

  static final categoriesCubit = CategoriesCubit(DataRepository.getRepository());

  static CategoriesBlocBuilder instance(){
    CategoriesBlocBuilder instance = CategoriesBlocBuilder();
    return instance;
  }

  Widget buildResult(){
    return BlocBuilder(
        cubit: categoriesCubit,
        builder: (BuildContext context, CategoriesState state){
          if(state is CategoriesLoaded){
            _onWidgetDidBuild((){
              Navigator.of(context).pushReplacementNamed('/home');
            });
            categoriesCubit.finish();
          }

          if(state is CategoriesLoading){

            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 100),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ],
            );

          }

          if(state is CategoriesError){

          }

          return Container();
        }
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

}