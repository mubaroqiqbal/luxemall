import 'package:flutter/material.dart';
import 'package:luxemall/cubit/products_cubit.dart';
import 'package:luxemall/data/data_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProdutsBlocBuilder{

  static final productsCubit = ProductsCubit(DataRepository.getRepository());

  static ProdutsBlocBuilder instance(){
    ProdutsBlocBuilder instance = ProdutsBlocBuilder();
    return instance;
  }

  Widget buildResult(){
    return BlocBuilder(
        cubit: productsCubit,
        builder: (BuildContext context, ProductsState state){
          if(state is ProductsLoaded){
            _onWidgetDidBuild((){
              Navigator.of(context).pushReplacementNamed('/home');
            });
            productsCubit.finish();
          }

          if(state is ProductsLoading){

            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 100),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ],
            );

          }

          if(state is ProductsError){

          }

          return Container();
        }
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

}