import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/cubit/products_cubit.dart';
import 'package:luxemall/util/constants_var.dart';
import 'package:luxemall/view/detail/detail_screen.dart';
import 'package:luxemall/view/home/bloc_builder/products_bloc_builder.dart';
import 'package:luxemall/view/home/components/sort.dart';

import 'categories.dart';
import 'item_card.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {

  ScrollController _controller = new  ScrollController();

  @override
  void initState() {
    super.initState();
    _controller.addListener(_scrollListener);
  }

  _scrollListener() {
    try{
      if (_controller.offset >= _controller.position.maxScrollExtent &&
          !_controller.position.outOfRange) {
        debugPrint("reach the bottom");
        ProdutsBlocBuilder.productsCubit.getProducts(true);
      }
    } catch (e, s) {
      debugPrint("error = "+e.toString());
      debugPrint("error = "+s.toString());
    }
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Categories(),
        Sort(),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
            child: BlocBuilder(
            cubit: ProdutsBlocBuilder.productsCubit,
            builder: (BuildContext context, ProductsState state){

              if(state is ProductsLoaded){
                return GridView.builder(
                    controller: _controller,
                    itemCount: state.responseProducts.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: defaultPadding,
                      crossAxisSpacing: defaultPadding,
                      childAspectRatio: 0.75,
                    ),
                    itemBuilder: (context, index) => ItemCard(
                      product: state.responseProducts[index],
                      press: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailsScreen(
                              product: state.responseProducts[index],
                            ),
                          )),
                    )
                );
              }
              if(state is ProductsLoading){

                return Center(
                  child: CircularProgressIndicator(),
                );

              }

              return Container();
            })
          ),
        ),
      ],
    );
  }
}
