import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/cubit/categories_cubit.dart';
import 'package:luxemall/util/constants_var.dart';
import 'package:luxemall/util/filter_box.dart';
import 'package:luxemall/view/home/bloc_builder/categories_bloc_builder.dart';
import 'package:luxemall/view/home/bloc_builder/products_bloc_builder.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {

  int selectedIndex = null;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: defaultPadding),
      child: SizedBox(
        height: 25,
        child: BlocBuilder(
            cubit: CategoriesBlocBuilder.categoriesCubit,
            builder: (BuildContext context, CategoriesState state){
              if(state is CategoriesLoaded){
                return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: state.responseCategories.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedIndex = index;
                          ProdutsBlocBuilder.productsCubit.temporaryFinish();
                          FilterBox.categoryFilter=state.responseCategories[index].toString();
                          ProdutsBlocBuilder.productsCubit.getProducts(false);
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              state.responseCategories[index].toString().toUpperCase(),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: selectedIndex == index ? textColor : textLightColor,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: defaultPadding / 4), //top padding 5
                              height: 2,
                              width: 30,
                              color: selectedIndex == index ? Colors.black : Colors.transparent,
                            )
                          ],
                        ),
                      ),
                    );
                  },
                );
              }

              if(state is CategoriesLoading){
                selectedIndex = null;
              }

              return Container();
            })
      ),
    );
  }

}
