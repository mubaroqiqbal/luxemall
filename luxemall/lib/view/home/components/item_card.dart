import 'package:flutter/material.dart';
import 'package:luxemall/data/model/product.dart';
import 'package:luxemall/util/constants_var.dart';

class ItemCard extends StatelessWidget {
  final Product product;
  final Function press;
  const ItemCard({
    Key key,
    this.product,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: itemBgColorTheme,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Image.network(product.image.replaceAll(".com", ".herokuapp.com"), fit: BoxFit.fill,)
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding / 4),
            child: Text(
              // products is out demo list
              product.title,
              style: TextStyle(color: textLightColor),
            ),
          ),
          Text(
            "\$${product.price}",
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
