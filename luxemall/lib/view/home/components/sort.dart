import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/cubit/categories_cubit.dart';
import 'package:luxemall/util/constants_var.dart';
import 'package:luxemall/util/filter_box.dart';
import 'package:luxemall/view/home/bloc_builder/categories_bloc_builder.dart';
import 'package:luxemall/view/home/bloc_builder/products_bloc_builder.dart';

// We need satefull widget for our categories

class Sort extends StatefulWidget {
  @override
  _SortState createState() => _SortState();
}

class _SortState extends State<Sort> {
  List<String> sortType = ["asc", "desc", "no filter"];
  int selectedIndex = null;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: defaultPadding),
      child: SizedBox(
          height: 25,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: sortType.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () async{
                  selectedIndex = index;
                  ProdutsBlocBuilder.productsCubit.temporaryFinish();
                  FilterBox.sortFilter=sortType[index];
                  ProdutsBlocBuilder.productsCubit.getProducts(false);
                  setState(() {});
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        sortType[index].toUpperCase(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: selectedIndex == index ? textColor : textLightColor,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: defaultPadding / 4), //top padding 5
                        height: 2,
                        width: 30,
                        color: selectedIndex == index ? Colors.black : Colors.transparent,
                      )
                    ],
                  ),
                ),
              );
            },
          )
      ),
    );
  }

}
