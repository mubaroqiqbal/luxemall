import 'package:flutter/material.dart';
import 'package:luxemall/data/model/product.dart';
import 'package:luxemall/util/constants_var.dart';

class ProductTitleWithImage extends StatelessWidget {
  const ProductTitleWithImage({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          children: [
            Expanded(
              child: Text(
                product.title,
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    .copyWith(color: textColor, fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
        SizedBox(height: defaultPadding),
        Container(
          height: size.height*0.4,
          width: size.height*0.4,
          child: Hero(
            tag: "${product.id}",
            child: Image.network(
              product.image.replaceAll(".com", ".herokuapp.com"),
              fit: BoxFit.contain,
            ),
          ),
        )
      ],
    );
  }
}
