import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/cubit/detail_product_cubit.dart';
import 'package:luxemall/data/model/product.dart';
import 'package:luxemall/util/constants_var.dart';
import 'package:luxemall/view/detail/components/price.dart';
import 'package:luxemall/view/detail/bloc_builder/detail_product_bloc_builder.dart';

import 'add_to_cart.dart';
import 'counter_with_fav_btn.dart';
import 'description.dart';
import 'product_title_with_image.dart';

class Body extends StatelessWidget {
  final Product product;

  const Body({Key key, this.product}) : super(key: key);
  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return SizedBox(
      height: size.height,
      width: size.width,
      child: Container(
        padding: EdgeInsets.only(
          top: defaultPadding,
          left: defaultPadding,
          right: defaultPadding,
        ),
        // height: 500,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
        child: SingleChildScrollView(
          child: BlocBuilder(
              cubit: DetailProductBlocBuilder.detailProductCubit,
              builder: (BuildContext context, DetailProductsState state){

                if(state is DetailProductsLoaded){
                  return Column(
                    children: <Widget>[
                      ProductTitleWithImage(product: state.product),
                      Price(product: state.product),
                      Description(product: state.product),
                      SizedBox(height: defaultPadding / 2),
                      CounterWithFavBtn(),
                      SizedBox(height: defaultPadding / 2),
                      AddToCart(product: state.product)
                    ],
                  );
                }

                if(state is DetailProductsLoading){

                  return Container(
                    height: size.height,
                    width: size.width,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );

                }

                return Container();
              })
        ),
      ),
    );
  }
}

