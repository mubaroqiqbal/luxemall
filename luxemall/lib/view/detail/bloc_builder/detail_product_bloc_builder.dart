import 'package:flutter/material.dart';
import 'package:luxemall/cubit/detail_product_cubit.dart';
import 'package:luxemall/cubit/products_cubit.dart';
import 'package:luxemall/data/data_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailProductBlocBuilder{

  static final detailProductCubit = DetailProductCubit(DataRepository.getRepository());

  static DetailProductBlocBuilder instance(){
    DetailProductBlocBuilder instance = DetailProductBlocBuilder();
    return instance;
  }

  Widget buildResult(){
    return BlocBuilder(
        cubit: detailProductCubit,
        builder: (BuildContext context, DetailProductsState state){
          if(state is DetailProductsLoaded){
            _onWidgetDidBuild((){
              Navigator.of(context).pushReplacementNamed('/home');
            });
            detailProductCubit.finish();
          }

          if(state is DetailProductsLoading){

            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 100),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ],
            );

          }

          if(state is ProductsError){

          }

          return Container();
        }
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

}