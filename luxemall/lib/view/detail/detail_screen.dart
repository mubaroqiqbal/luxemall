import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:luxemall/cubit/detail_product_cubit.dart';
import 'package:luxemall/data/model/product.dart';
import 'package:luxemall/util/constants_var.dart';
import 'package:luxemall/view/detail/components/body.dart';
import 'package:luxemall/view/detail/bloc_builder/detail_product_bloc_builder.dart';

class DetailsScreen extends StatefulWidget {
  final Product product;

  const DetailsScreen({Key key, this.product}) : super(key: key);

  @override
  _DetailsScreen createState() => _DetailsScreen(product: product);
}

class _DetailsScreen extends State<DetailsScreen>{
  final Product product;

  _DetailsScreen({Key key, this.product});

  @override
  void initState() {
    super.initState();
    DetailProductBlocBuilder.detailProductCubit.getProduct(product.id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: buildAppBar(context),
      body: Body(product: product),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: primaryGold,
      elevation: 0,
      leading: IconButton(
        icon: SvgPicture.asset(
          'assets/icons/back.svg',
          color: Colors.white,
        ),
        onPressed: () => Navigator.pop(context),
      ),
      actions: <Widget>[
        IconButton(
          icon: SvgPicture.asset("assets/icons/cart.svg"),
          onPressed: () {},
        ),
        SizedBox(width: defaultPadding / 2)
      ],
    );
  }
}
