import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:luxemall/data/data_repository.dart';

part 'categories_state.dart';

class CategoriesCubit extends Cubit<CategoriesState> {
  final DataRepository _dataRepository;
  static int dataLength=0;

  CategoriesCubit(this._dataRepository) : super(CategoriesInitial());

  Future<void> getCategories() async {
    try {
      emit(CategoriesLoading());

      final listCategories = await _dataRepository.getCategory();

      if(listCategories.length<=0){
        debugPrint("empty");
        emit(CategoriesEmptyData("Data Tidak Ditemukan"));
      }else{
        debugPrint("loaded");
        emit(CategoriesLoaded(listCategories));
      }
    } on SocketException {
      emit(CategoriesError("Please Check Your Internet Or Try Again Later"));
    } on Exception catch(e){
      emit(CategoriesError(e.toString().replaceFirst("Exception: ", "")));
    }
  }
  Future<void> finish() async{
    emit(CategoriesInitial());
  }

}