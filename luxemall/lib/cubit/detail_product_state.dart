part of 'detail_product_cubit.dart';

abstract class DetailProductsState {
  const DetailProductsState();
}

class DetailProductsInitial extends DetailProductsState {
  const DetailProductsInitial();
}

class DetailProductsLoading extends DetailProductsState {
  const DetailProductsLoading();
}

class DetailProductsLoaded extends DetailProductsState {

  final Product product;
  final CartsResponse cartsResponse;

  const DetailProductsLoaded(this.product, this.cartsResponse);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is DetailProductsLoaded && o.product == product&& o.cartsResponse == cartsResponse;
  }

  @override
  int get hashCode => product.hashCode;
}

class DetailProductsSuccessAddCart extends DetailProductsState {

  final CartsResponse product;

  const DetailProductsSuccessAddCart(this.product);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is DetailProductsSuccessAddCart && o.product == product;
  }

  @override
  int get hashCode => product.hashCode;
}

class DetailProductsError extends DetailProductsState {

  final String message;
  const DetailProductsError(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is DetailProductsError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}

class DetailProductsEmptyData extends DetailProductsState {

  final String message;
  const DetailProductsEmptyData(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is DetailProductsEmptyData && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}