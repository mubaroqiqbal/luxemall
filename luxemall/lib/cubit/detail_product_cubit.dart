import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:luxemall/data/data_repository.dart';
import 'package:luxemall/data/model/CartsResponse.dart';
import 'package:luxemall/data/model/cart_product.dart';
import 'package:luxemall/data/model/carts.dart';
import 'package:luxemall/data/model/product.dart';

part 'detail_product_state.dart';

class DetailProductCubit extends Cubit<DetailProductsState> {
  final DataRepository _dataRepository;

  Product productTemp;

  DetailProductCubit(this._dataRepository) : super(DetailProductsInitial());

  Future<void> getProduct(String productId) async {
    try {
      emit(DetailProductsLoading());

      final product = await _dataRepository.getDetailProduct(productId);

      productTemp = product;

      if(product==null){
        debugPrint("empty");
        emit(DetailProductsEmptyData("Data Tidak Ditemukan"));
      }else{
        debugPrint("loaded");
        emit(DetailProductsLoaded(product, null));
      }
    } on SocketException {
      emit(DetailProductsError("Please Check Your Internet Or Try Again Later"));
    } on Exception catch(e){
      emit(DetailProductsError(e.toString().replaceFirst("Exception: ", "")));
    }
  }

  Future<void> addCart(int productId, int quantity) async {
    try {

      emit(DetailProductsLoading());

      CartProduct cartProduct = new CartProduct(productId: productId, quantity: quantity);

      String date = DateTime.now().year.toString()+"-"+DateTime.now().month.toString()+"-"+DateTime.now().day.toString();
      Carts param = new Carts(
        userId: "1",
        date: date,
        products: cartProduct,
      );

      final product = await _dataRepository.addCart(param);

      if(product==null){
        debugPrint("empty");
        emit(DetailProductsEmptyData("Data Tidak Ditemukan"));
      }else{
        debugPrint("loaded");
        emit(DetailProductsLoaded(productTemp, product));
      }
    } on SocketException {
      emit(DetailProductsError("Please Check Your Internet Or Try Again Later"));
    } on Exception catch(e){
      emit(DetailProductsError(e.toString().replaceFirst("Exception: ", "")));
    }
  }

  Future<void> finish() async{
    emit(DetailProductsInitial());
  }

}