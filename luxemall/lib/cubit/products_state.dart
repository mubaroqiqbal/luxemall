part of 'products_cubit.dart';

abstract class ProductsState {
  const ProductsState();
}

class ProductsInitial extends ProductsState {
  const ProductsInitial();
}

class ProductsLoading extends ProductsState {
  const ProductsLoading();
}

class ProductsLoaded extends ProductsState {

  final List<Product> responseProducts;

  const ProductsLoaded(this.responseProducts);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ProductsLoaded && o.responseProducts == responseProducts;
  }

  @override
  int get hashCode => responseProducts.hashCode;
}

class ProductsError extends ProductsState {

  final String message;
  const ProductsError(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ProductsError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}

class ProductsEmptyData extends ProductsState {

  final String message;
  const ProductsEmptyData(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ProductsEmptyData && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}