import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:luxemall/data/data_repository.dart';
import 'package:luxemall/data/model/product.dart';
import 'package:luxemall/util/filter_box.dart';

part 'products_state.dart';

class ProductsCubit extends Cubit<ProductsState> {
  final DataRepository _dataRepository;
  static List<Product> responseProducts = new List<Product>();

  ProductsCubit(this._dataRepository) : super(ProductsInitial());

  Future<void> getProducts(bool isScroll) async {
    try {
      emit(ProductsLoading());

      final listProducts = await _dataRepository.getAllProduct(FilterBox.sortFilter, FilterBox.categoryFilter);

      if(listProducts.length<=0){
        debugPrint("empty");
        emit(ProductsEmptyData("Data Tidak Ditemukan"));
      }else{
        debugPrint("loaded");

        if(isScroll){
          responseProducts.addAll(listProducts);
          emit(ProductsLoaded(responseProducts));
        }else{
          emit(ProductsLoaded(listProducts));
        }
      }
    } on SocketException {
      emit(ProductsError("Please Check Your Internet Or Try Again Later"));
    } on Exception catch(e){
      emit(ProductsError(e.toString().replaceFirst("Exception: ", "")));
    }
  }

  Future<void> temporaryFinish() async{
    emit(ProductsInitial());
  }

  Future<void> finish() async{
    FilterBox.sortFilter="";
    FilterBox.categoryFilter=  "";
    emit(ProductsInitial());
  }

}