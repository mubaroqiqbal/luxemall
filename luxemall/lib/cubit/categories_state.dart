part of 'categories_cubit.dart';

abstract class CategoriesState {
  const CategoriesState();
}

class CategoriesInitial extends CategoriesState {
  const CategoriesInitial();
}

class CategoriesLoading extends CategoriesState {
  const CategoriesLoading();
}

class CategoriesLoaded extends CategoriesState {

  final List<dynamic> responseCategories;

  const CategoriesLoaded(this.responseCategories);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CategoriesLoaded && o.responseCategories == responseCategories;
  }

  @override
  int get hashCode => responseCategories.hashCode;
}

class CategoriesError extends CategoriesState {

  final String message;
  const CategoriesError(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CategoriesError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}

class CategoriesEmptyData extends CategoriesState {

  final String message;
  const CategoriesEmptyData(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CategoriesEmptyData && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}