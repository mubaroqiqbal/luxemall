import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as ioClient;
import 'package:luxemall/data/model/CartsResponse.dart';
import 'package:luxemall/data/model/carts.dart';
import 'package:luxemall/data/model/product.dart';
import 'package:luxemall/data/model/user_data.dart';

class DataRepository {

  static DataRepository _dataRepository;

  String baseServerUrl = "https://fakestoreapi.herokuapp.com/";

  String getAllProductPath = "products";
  String getDetailProductPath = "products/";
  String getProductCategoryPath = "products/categories";
  String getProductByCategoryPath = "products/category/";
  String getCartByUserPath = "carts/user/";
  String addCartPath = "carts";
  String registerUserPath = "users";
  String getUserPath = "users/";

  DataRepository();

  static DataRepository getRepository() {
    if (_dataRepository != null) {
      return _dataRepository;
    } else {
      _dataRepository = DataRepository();
      return _dataRepository;
    }
  }

  Future<List<Product>> getAllProduct(String sortFilter, String categoryFilter) async {
    String filter = "";

    if(categoryFilter!=null && categoryFilter!=""){
      filter=filter+"/category/"+categoryFilter;
    }

    filter=filter+"?limit=6";

    if(sortFilter=="asc"){
      filter=filter+"&sort=asc";
    }else if(sortFilter=="desc"){
      filter=filter+"&sort=desc";
    }

    String url = baseServerUrl + getAllProductPath + filter;
    debugPrint(url);

    final response = await ioClient.get(url);

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 400) {
      throw Exception(response.body);
    } else if (response.statusCode == 200 || response.statusCode == 201) {

      Iterable list = json.decode(response.body);

      return list.map((model) => Product.fromJson(model)).toList();
    } else {
      throw Exception("Other Error");
    }
  }

  Future<Product> getDetailProduct(String prdoductId) async {

    String url = baseServerUrl + getDetailProductPath + prdoductId;
    debugPrint(url);

    final response = await ioClient.get(url);

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 400) {
      throw Exception(response.body);
    } else if (response.statusCode == 200 || response.statusCode == 201) {

      return Product.fromJson(json.decode(response.body));
    } else {
      throw Exception("Other Error");
    }
  }

  Future<List<dynamic>> getCategory() async {

    String url = baseServerUrl + getProductCategoryPath;

    final response = await ioClient.get(
        url,
    );

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 400) {
      throw Exception(response.body);
    } else if (response.statusCode == 200 || response.statusCode == 201) {

      Iterable list = json.decode(response.body);

      return list.toList();
    } else {
      throw Exception("Other Error");
    }
  }

  Future<List<Product>> getProductByCategory(String category, String filter) async {

    if(filter!=""){
      filter=filter+"&limit=6";
    }else{
      filter=filter+"?limit=6";
    }

    String url = baseServerUrl + getProductByCategoryPath + category + filter;

    final response = await ioClient.get(
        url,
    );

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 400) {
      throw Exception(response.body);
    } else if (response.statusCode == 200) {
      Iterable list = json.decode(response.body);

      return list.map((model) => Product.fromJson(model)).toList();
    } else {
      throw Exception("Other Error");
    }
  }

  Future<void> getCartByUser(String user) async {
    Map<String, String> header = {};

    String url = baseServerUrl + getCartByUserPath + user;

    final response = await ioClient.get(
        url,
        headers: header,
    );

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 400) {
      throw Exception(response.body);
    } else if (response.statusCode == 200) {

    } else {
      throw Exception("Other Error");
    }
  }

  Future<void> registerUser(UserData user) async {
    Map<String, String> header = {};

    String url = baseServerUrl + registerUserPath;

    final response = await ioClient.post(
        url,
        headers: header,
        body: json.encode(user)
    );

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 400) {
      throw Exception(response.body);
    } else if (response.statusCode == 200) {

    } else {
      throw Exception("Other Error");
    }
  }

  Future<void> getUser(String userId) async {
    Map<String, String> header = {};

    String url = baseServerUrl + getUserPath + userId;

    final response = await ioClient.get(
        url,
        headers: header,
    );

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 400) {
      throw Exception(response.body);
    } else if (response.statusCode == 200) {

    } else {
      throw Exception("Other Error");
    }
  }

  Future<CartsResponse> addCart(Carts param) async {
    Map<String, String> header = {};

    String url = baseServerUrl + addCartPath;

    final response = await ioClient.post(
        url,
        headers: header,
        body: json.encode(param)
    );

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 400) {
      throw Exception(response.body);
    } else if (response.statusCode == 200) {

      return CartsResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception("Other Error");
    }
  }

}