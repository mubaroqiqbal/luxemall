import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:luxemall/data/model/cart_product.dart';

class CartsResponse {

  int id;
  String userId;
  var date;
  List<CartProduct> products;

  CartsResponse({Key key, this.id, this.userId, this.date, this.products});

  CartsResponse.fromJson(Map<String, dynamic> data) {
    id = data['id'];
    userId = data['userId'];
    date = data['date'];
    Iterable list = json.decode(data['products'].toString());
    products = list.map((model) => CartProduct.fromJson(model)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['date'] = this.date;
    data['products'] = json.encode(this.products);
    return data;
  }
}