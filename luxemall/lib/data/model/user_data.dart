import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:luxemall/data/model/address_data.dart';
import 'package:luxemall/data/model/name_data.dart';

class UserData {

  int id;
  String email;
  String username;
  String password;
  NameData name;
  AddressData address;
  String phone;

  UserData({Key key, this.id, this.username, this.email, this.password, this.name, this.phone, this.address});

  UserData.fromJson(Map<String, dynamic> data) {
    id = data['id'];
    username = data['username'];
    email = data['email'];
    password = data['password'];
    name = data['name']!=null? NameData.fromJson(json.decode(data['name'].toString())) : data['name'];
    phone = data['phone'];
    address = data['address']!=null? AddressData.fromJson(json.decode(data['address'].toString())) : data['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['email'] = this.email;
    data['password'] = this.password;
    data['username'] = this.username;
    data['name'] = json.encode(this.name);
    data['phone'] = this.phone;
    data['address'] = json.encode(this.address);
    return data;
  }
}