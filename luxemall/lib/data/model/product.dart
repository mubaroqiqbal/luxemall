import 'package:flutter/material.dart';

class Product {

  int id;
  String title;
  double price;
  String category;
  String description;
  String image;

  Product({Key key, this.id, this.title, this.image, this.description, this.category, this.price});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    description = json['description'];
    category = json['category'];
    price = json['price'].toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    data['description'] = this.description;
    data['category'] = this.category;
    data['price'] = this.price;
    return data;
  }
}