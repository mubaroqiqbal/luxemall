import 'package:flutter/material.dart';

class NameData {

  String firstname;
  String lastname;

  NameData({Key key, this.firstname, this.lastname});

  NameData.fromJson(Map<String, dynamic> json) {
    firstname = json['firstname'];
    lastname = json['lastname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    return data;
  }
}