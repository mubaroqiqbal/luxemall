import 'package:flutter/material.dart';

class AddressData {

  String city;
  String street;
  int number;
  String zipcode;
  String geolocation;
  String lat;
  String long;

  AddressData({Key key, this.city, this.street, this.geolocation, this.lat, this.long, this.number, this.zipcode});

  AddressData.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    street = json['street'];
    geolocation = json['geolocation'];
    lat = json['lat'];
    long = json['long'];
    number = json['number'];
    zipcode = json['zipcode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['street'] = this.street;
    data['geolocation'] = this.geolocation;
    data['lat'] = this.lat;
    data['long'] = this.long;
    data['number'] = this.number;
    data['zipcode'] = this.zipcode;
    return data;
  }
}
