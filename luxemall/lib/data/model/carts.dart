import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:luxemall/data/model/cart_product.dart';

class Carts {

  int id;
  String userId;
  var date;
  CartProduct products;

  Carts({Key key, this.id, this.userId, this.date, this.products});

  Carts.fromJson(Map<String, dynamic> data) {
    id = data['id'];
    userId = data['userId'];
    date = data['date'];
    products = data['products']!=null? CartProduct.fromJson(json.decode(data['products'].toString())) : data['products'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['date'] = this.date;
    data['products'] = json.encode(this.products);
    return data;
  }
}