import 'package:flutter/material.dart';

const textColor = Color.fromRGBO(88, 88, 88, 1);
const buttonColor = Color.fromRGBO(88, 88, 88, 1);
const textLightColor = Color.fromRGBO(88, 88, 88, 0.5);
const primaryGold = Color.fromRGBO(194, 145, 46, 1);
const itemBgColorTheme = Color.fromRGBO(88, 88, 88, 0.5);

const defaultPadding = 20.0;
